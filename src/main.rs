extern crate clap;
pub mod print;
pub mod query;

use clap::{Arg, ArgAction, Command};
use query::*;

fn main() {
    let cmd = Command::new("pacdeps")
        .subcommand_required(true)
        .subcommand(
            Command::new("expdep")
                .long_flag("expdep")
                .short_flag('E')
                .about("list packages that are both explicit and dependencies"),
        )
        .subcommand(
            Command::new("opt")
                .long_flag("opt")
                .short_flag('O')
                .about("list all optional packages"),
        )
        .subcommand(
            Command::new("depopt")
                .long_flag("depopt")
                .short_flag('D')
                .about("list all optional dependencies"),
        )
        .subcommand(
            Command::new("lsoptfor")
                .long_flag("lsoptfor")
                .short_flag('F')
                .about("list all installed packages this is or could be optional for")
                .arg(
                    Arg::new("package")
                        .help("packages")
                        .required(true)
                        .action(ArgAction::Set)
                        .num_args(1),
                ),
        )
        .arg(
            Arg::new("verbose")
                .long("verbose")
                .short('v')
                .help("print more information")
                .num_args(0)
                .global(true),
        )
        .arg(
            Arg::new("stats")
                .long("stats")
                .short('s')
                .help("print some statistics at the end")
                .num_args(0)
                .conflicts_with("only-stats")
                .global(true),
        )
        .arg(
            Arg::new("only-stats")
                .long("only-stats")
                .short('S')
                .help("print only the statistics")
                .num_args(0)
                .conflicts_with("stats")
                .global(true),
        )
        .get_matches();

    match cmd.subcommand() {
        Some(("expdep", cmd)) => {
            expdep(cmd);
        }
        Some(("opt", cmd)) => {
            opt(cmd);
        }
        Some(("depopt", cmd)) => {
            depopt(cmd);
        }
        Some(("lsoptfor", cmd)) => {
            lsoptfor(cmd);
        }
        _ => unreachable!(),
    }
}
