extern crate alpm;
extern crate clap;

use self::alpm::{Alpm, PackageReason};
use self::clap::ArgMatches;
use print::*;

pub fn expdep(cmd: &ArgMatches) {
    let handle = Alpm::new("/", "/var/lib/pacman").unwrap();
    let db = handle.localdb();
    let mut all_exp = 0;
    let mut are_dep = 0;
    let mut are_req_dep = 0;
    let mut are_opt_dep = 0;
    for pkg in db.pkgs() {
        if pkg.reason() == PackageReason::Explicit {
            let mut hasdeps = false;
            let reqby = pkg.required_by();
            let opfor = pkg.optional_for();

            all_exp += 1;

            if !reqby.is_empty() {
                are_req_dep += 1;
                are_dep += 1;
                hasdeps = true;
            } else if !opfor.is_empty() {
                are_opt_dep += 1;
                are_dep += 1;
                hasdeps = true;
            }

            if !hasdeps {
                continue;
            }

            if !cmd.get_flag("only-stats") {
                if cmd.get_flag("verbose") {
                    println!("Name            : {}", pkg.name());
                    print_str_multi("Descrption      :".to_string(), pkg.desc().unwrap());
                    if reqby.is_empty() {
                        println!("Required By     : -");
                    } else {
                        print_list_multi("Required By     :".to_string(), reqby);
                    }
                    if opfor.is_empty() {
                        println!("Optional For    : -");
                    } else {
                        print_list_multi("Optional For    :".to_string(), opfor);
                    }
                    print_dashes();
                } else {
                    println!("{}", pkg.name());
                }
            }
        }
    }
    if cmd.get_flag("stats") {
        println!();
        println!("---[STATS]---");
    }
    if cmd.get_flag("stats") || cmd.get_flag("only-stats") {
        print_stats_expdep(all_exp, are_dep, are_req_dep, are_opt_dep);
    }
}

pub fn opt(cmd: &ArgMatches) {
    let handle = Alpm::new("/", "/var/lib/pacman").unwrap();
    let db = handle.localdb();
    let mut are_opt_dep = 0;
    for pkg in db.pkgs() {
        let reqby = pkg.required_by();
        let opfor = pkg.optional_for();

        if reqby.is_empty() && !opfor.is_empty() {
            are_opt_dep += 1;
            if !cmd.get_flag("only-stats") {
                if cmd.get_flag("verbose") {
                    println!("Name            : {}", pkg.name());
                    print_str_multi("Descrption      :".to_string(), pkg.desc().unwrap());
                    print_list_multi("Optional For    :".to_string(), opfor);
                    print_dashes();
                } else {
                    println!("{}", pkg.name());
                }
            }
        }
    }
    if cmd.get_flag("stats") {
        println!();
        println!("---[STATS]---");
    }
    if cmd.get_flag("stats") || cmd.get_flag("only-stats") {
        print_stats_opt(are_opt_dep);
    }
}

pub fn depopt(cmd: &ArgMatches) {
    let handle = Alpm::new("/", "/var/lib/pacman").unwrap();
    let db = handle.localdb();
    let mut are_opt_dep = 0;
    for pkg in db.pkgs() {
        if pkg.reason() == PackageReason::Depend {
            let reqby = pkg.required_by();
            let opfor = pkg.optional_for();

            if reqby.is_empty() && !opfor.is_empty() {
                are_opt_dep += 1;
                if !cmd.get_flag("only-stats") {
                    if cmd.get_flag("verbose") {
                        println!("Name            : {}", pkg.name());
                        print_str_multi("Descrption      :".to_string(), pkg.desc().unwrap());
                        print_list_multi("Optional For    :".to_string(), opfor);
                        print_dashes();
                    } else {
                        println!("{}", pkg.name());
                    }
                }
            }
        }
    }
    if cmd.get_flag("stats") {
        println!();
        println!("---[STATS]---");
    }
    if cmd.get_flag("stats") || cmd.get_flag("only-stats") {
        print_stats_depopt(are_opt_dep);
    }
}

pub fn lsoptfor(cmd: &ArgMatches) {
    let handle = Alpm::new("/", "/var/lib/pacman").unwrap();
    let db = handle.localdb();
    let user_dep: &String = cmd
        .get_one::<String>("package")
        .expect("lsoptfor get package");
    let mut is_installed = false;
    let mut is_opt_dep = 0;

    for pkg in db.pkgs() {
        if user_dep == pkg.name() {
            is_installed = true;
        }
    }

    for pkg in db.pkgs() {
        for dep in pkg.optdepends() {
            if user_dep == dep.name() {
                is_opt_dep += 1;
                if !cmd.get_flag("only-stats") {
                    if cmd.get_flag("verbose") {
                        println!("Name            : {}", pkg.name());
                        print_str_multi("Descrption      :".to_string(), pkg.desc().unwrap());
                        if is_installed {
                            println!("                : {} [installed]", dep);
                        } else {
                            println!("                : {}", dep);
                        }
                        print_dashes();
                    } else {
                        println!("{}", pkg.name());
                    }
                }
                break;
            }
        }
    }
    if cmd.get_flag("stats") {
        println!();
        println!("---[STATS]---");
    }
    if cmd.get_flag("stats") || cmd.get_flag("only-stats") {
        println!("is listed as optional for {} packages", is_opt_dep);
    }
}
