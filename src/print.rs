extern crate alpm;
extern crate terminal_size;

use self::alpm::AlpmListMut;
use self::terminal_size::{terminal_size, Width};

pub fn print_dashes() {
    if let Some((Width(w), _)) = terminal_size() {
        for _ in 1..w {
            print!("-");
        }
        println!();
    } else {
        println!();
    }
}

pub fn print_str_multi(title: String, ldep: &str) {
    let padding: String = "                ".to_string();
    let term_size = terminal_size();
    if let Some((Width(w), _)) = term_size {
        let mut idep = ldep.split(' ');
        let mut str: String = title;
        let mut temp: String = str.clone() + " " + idep.next().unwrap();
        for el in idep {
            temp = temp.clone() + " " + el;
            if temp.len() + 1 > usize::from(w) {
                println!("{}", str);
                str = padding.clone() + "  " + el;
                temp = padding.clone() + "  " + el;
            } else {
                str = temp.clone();
            }
        }
        str = temp.clone();
        println!("{}", str);
    } else {
        println!("{} {}", title, ldep);
    }
}

pub fn print_list_multi(title: String, ldep: AlpmListMut<String>) {
    let padding: String = "                ".to_string();
    let term_size = terminal_size();
    if let Some((Width(w), _)) = term_size {
        let mut idep = ldep.iter();
        let mut str: String = title;
        let mut temp: String = str.clone() + " " + idep.next().unwrap();
        for el in idep {
            temp = temp.clone() + "  " + el;
            if temp.len() + 1 > usize::from(w) {
                println!("{}", str);
                str = padding.clone() + "  " + el;
                temp = padding.clone() + "  " + el;
            } else {
                str = temp.clone();
            }
        }
        str = temp.clone();
        println!("{}", str);
    } else {
        for el in ldep {
            print!("{} {}", title, el);
        }
        println!();
    }
}

pub fn print_stats_expdep(all_exp: usize, are_dep: usize, are_req_dep: usize, are_opt_dep: usize) {
    println!("{} explicitly installed", all_exp);
    println!("└──{} also dependencies", are_dep);
    println!("   ├──{} required", are_req_dep);
    println!("   └──{} optional", are_opt_dep);
}

pub fn print_stats_opt(are_opt_dep: usize) {
    println!("{} packages are optional", are_opt_dep);
}

pub fn print_stats_depopt(are_opt_dep: usize) {
    println!("{} packages are just optional dependencies", are_opt_dep);
}
